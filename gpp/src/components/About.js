import React from 'react';
import {Container} from 'semantic-ui-react';


const About = () => (
  <Container>
    <h1>About</h1>
    <h3>Vestibulum id Ligula Porta Felis Euismod Semper</h3>
    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nullam quis
      risus eget urna mollis ornare vel eu leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis
      consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit.
    </p>
    <p>
      Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nullam quis risus
      eget urna mollis ornare vel eu leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi leo risus,
      porta
      ac consectetur ac, vestibulum at eros.
    </p>
    <p>
      Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit
      aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Lorem
      ipsum dolor sit amet, consectetur adipiscing elit. Curabitur blandit tempus porttitor.
    </p>
    <p>
      Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Donec ullamcorper nulla non metus auctor
      fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
    </p>
  </Container>
);

export default About;
