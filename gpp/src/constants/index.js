export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';
export const REGISTER_USER = 'REGISTER_USER';
export const DEREGISTER_USER = 'DEREGISTER_USER';
export const APPROVE_USER = 'APPROVE_USER';
export const DENY_USER = 'DENY_USER';
